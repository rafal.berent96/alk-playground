# Terraform

## Prerequisites

W projekcie, w którym będziemy provisionować zasoby należy stworzyć konto serwisowe z odpowiednimi uprawnieniami oraz pobrać jego credentiale i wrzucić do katalogów **ansible** oraz **terraform**

Minimalne predefiniowane uprawnienia dla konta serwisowego:
- roles/compute.instanceAdmin
- roles/compute.networkAdmin
- roles/compute.securityAdmin

## Konfiguracja providera
```
provider "google" {
  project      = "nazwa projektu"
  credentials  = file("./credentials.json")
  region       = var.gcp_region
  zone         = var.gcp_zone
}
```
Wymagane parametry:
- **project** - Nazwa projektu, w ramach którego tworzymy zasoby
- **credentials** - ścieżka do pliku z credentialami konta serwisowego.

Pozostałe parametry jak **region** oraz **zone** posiadają defaultowe wartości określone w pliku **variables**.tf

## Konfiguracja instancji Compute Instance
```
resource "google_compute_instance" "alkdemo" {
  count        = var.instance_count
  name         = "alk-demo-${count.index}"
  machine_type = var.gcp_machine_type

  tags = ["alk", "http-server"]
  labels = {
    env = "prod"
  }
  boot_disk {
    auto_delete = true
    device_name = "alkdemo_${count.index}_disk"

    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
      size  = 30
      type  = "pd-standard"
    }
    
    mode = "READ_WRITE"
  }

  network_interface {
    network = google_compute_network.vpc_network.self_link
    access_config {
      // for public ip
    }
  }

  connection {
    type  = "ssh"
    user  = var.username
    host  = self.network_interface[0].access_config[0].nat_ip
    private_key = file(var.ssh_private_key_filepath)
  }

  provisioner "file" {
    source      = "./provision_files/script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "/tmp/script.sh",
    ]
  }

  metadata = {
    sshKeys = "${var.username}:${file(var.ssh_public_key_filepath)}"
  }
  allow_stopping_for_update = true
}
```

W ramach podstawowej konfiguracji instancji Compute Instance korzystamy z następujących parametrów zdefiniowanych w **variables.tf**:
- instance_count - określa ile instancji tego samego typu ma zostać powołanych
- machine_type - jakiego typu mają być tworzone instancje

Ustawione zostały również **tagi** oraz **labels**. **Labelki** wykorzystywane są w ramach **Ansible Dynamic Inventory**

Kolejnym blokiem jest **boot_disk**, w ramach którego zdefiniowany został system operacyjny oraz wielkość i rodzaj dysku systemowego.

W bloku **network_interface** deklarujemy w jakiej VPC mają zostać utworzone instancje. Parametr **network** przyjmuje nazwę VPC. W tym przypadku wykorzystana została zmienna **google_compute_network.vpc_network.self_link**, która wskazuje bezpośrednio na sieć tworzoną w ramach tego skryptu, gdzie **vpc_network** to nazwa resource'a.

Bloki **connection, provisioner "file", provisioner "remote-exec** oraz **metadata** zostały użyte w celu wykorzystania skryptu do instalacji i konfiguracji Nginxa na każdej instancji. 

- **Connection** określa sposób połączenia się do stworzonej maszyny wirtualnej. W ramach tej części kodu wskazujemy parametry:
- - type - metoda połączenia
- - user - jaki użytkownik ma zostać wykorzystany - w tym przypadku nazwa usera jest określona w **variables.tf**. Ten user zostanie stworzony.
- - host - przyjmuje adres IP maszyny, do której dany user ma się połączyć. W przypadku tworzenia wielu identycznych instancji należy wykorzysać parametr **self.network_interface[0].access_config[0].nat_ip** aby wskazać adres zewnętrzny maszyny, która w danej chwili jest provisionowana.
- - private_key - klucz prywatny, który umożliwi logowanie się z użyciem klucza ssh. W bloku **metadata** wskazany jest klucz publiczny, który zostaje przekazany do maszyny.

- **Provisioner "file"** - w tym bloku wskazujemy, który plik chcemy wrzucić na tworzoną maszynę
- **Provisioner "remote-exec"** - ten blok natomiast odpowiada, za wykonanie komendy na maszynie - w tym przypadku jest to nadanie uprawnie do uruchamiania oraz uruchomienie skryptu.

## Konfiguracja VPC
```
resource "google_compute_network" "vpc_network" {
  name                    = "terraform-network"
  auto_create_subnetworks = "true"
}
```
Podstawowa konfiguracja **VPC**, w której sieć o wskazanej nazwie z automatycznym tworzeniem podsieci we wszystkich regionach.

## Konfiguracja ustawień FW
```
resource "google_compute_firewall" "allow_ssh_http" {
  name    = "allow-ssh-http"
  network = google_compute_network.vpc_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["22", "80"]
  }

  source_ranges = ["0.0.0.0/0"]
}
```
Reguła firewalla o nazwie **allow-ssh-http**, która jak nazwa wskazuje zezwala na ruch po portach 22 oraz 80. Ponownie zamiast podawać konkretną nazwę VPC wykorzystujemy wskaźnik na zasób o nazwie **vpc_network**.

## Konfiguracja LoadBalancera
### Unmanaged Instance Group
```
resource "google_compute_instance_group" "instance_group" {
  name        = "alkdemo-instance-group"
  description = "Alkdemo Instance Group"

  instances = google_compute_instance.alkdemo[*].self_link

  named_port {
    name   = "alkdemo-http"
    port   = 80
  }
}
```
Z uwagi, że nie został użyty Instance Template, stworzona została **Unmanaged Instance Group** w ramach której wskazane zostały wszystkie stworzone instance Compute Instance. Zadeklarowany został również **named_port**, który później zostanie wykorzystany przy wskazywaniu portu w **backend_service**

### Backend Service
```
resource "google_compute_region_backend_service" "backend_service" {
  name        = "alkdemo-backend-service"
  description = "Alkdemo Backend Service for Load Balancer"

  port_name   = "alkdemo-http"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  locality_lb_policy = "ROUND_ROBIN"
  protocol    = "HTTP"
  timeout_sec = 30
  
  health_checks = [google_compute_region_health_check.health_check.self_link]
  backend {
    group = google_compute_instance_group.instance_group.self_link
    balancing_mode = "UTILIZATION"
    capacity_scaler = 1
    max_utilization = 0.8
  }
}
```
**Regional Backend Service** wskazuje backend dla LoadBalancera. W tym przypadku typ loadbalancera to **External Managed** (Regional External Application Load Balancer). Backend'em jest wcześniej stworzona **Instance Group**. Podstawowa konfiguracja backendu dla loadbalancera wymaga również zadeklarowania **health check'a**
### Health check
```
resource "google_compute_region_health_check" "health_check" {
  name               = "alkdemo-health-check"
  check_interval_sec = 10
  timeout_sec        = 10
  unhealthy_threshold = 10
  healthy_threshold   = 2
  
  http_health_check {
    port               = 80
  }
  log_config {
    enable = true
  }
}
```
Podstawowy http health check, który sprawdza, czy elementy backendu (w tym przypadku wszystkie instance w stworzonej Instance Group) żyją i odpowiadają na porcie 80
### Proxy-only Subnet
```
resource "google_compute_subnetwork" "proxy_subnet" {
  name          = "alkdemo-proxy-subnet"
  ip_cidr_range = "10.0.0.0/24"
  purpose       = "REGIONAL_MANAGED_PROXY"
  role          = "ACTIVE"
  network       = google_compute_network.vpc_network.self_link
  region        = var.gcp_region
}
```
Proxy dla Loadbalancera wymaga oddzielnej podsieci typu Proxy_only_subnet.
### Firewall rule - Allow Loadbalacner Traffic
```
resource "google_compute_firewall" "allow_lb_traffic" {
  name    = "allow-lb-traffic"
  network = google_compute_network.vpc_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = [google_compute_subnetwork.proxy_subnet.ip_cidr_range]
}
```
Reguła, która zezwala na ruch po porcie 80 do serwera proxy.
### URL Map / Load Balancer
```
resource "google_compute_region_url_map" "url_map" {
  name        = "alkdemo-lb-url-map"
  default_service = google_compute_region_backend_service.backend_service.self_link
}
```
URL Map, definuje do którego **backend service** ma trafić zapytanie. W polu **default_service** wskazany jest backend_service - resource odpowiadający za Regional Backend Service stworzony wcześniej.
### Target HTTP Proxy
```
resource "google_compute_region_target_http_proxy" "target_proxy" {
  name        = "alkdemo-lb-target-proxy"
  url_map     = google_compute_region_url_map.url_map.self_link
  region      = var.gcp_region
}
```
Proxy nawiązuje połączenie z odpowiednim backend service'em za pomocą **url_map** zdefiniowanej wcześniej.
### Forwarding Rule
```
resource "google_compute_forwarding_rule" "forwarding_rule" {
  name               = "alkdemo-lb-forwarding-rule"
  region             = var.gcp_region
  load_balancing_scheme = "EXTERNAL_MANAGED"
  target             = google_compute_region_target_http_proxy.target_proxy.self_link
  network            = google_compute_network.vpc_network.self_link
  ip_protocol        = "TCP"
  port_range         = "80"
}
```
**Forwarding rule** odbiera zapytania od klienta na wskazanym porcie i przekazuje do konkretnego **target proxy**
## Output
```
output "external_vm_ip" {
  value = ["${google_compute_instance.alkdemo.*.network_interface.0.access_config.0.nat_ip}"]
}
output "external_lb_ip" {
  value = google_compute_forwarding_rule.forwarding_rule.ip_address
}

```
Jako wynik poprawnego wykonania całej operacji wyświetlamy dodatkowo zewnętrzne adresy wszystkich instancji, które zostały stworzone oraz adres load balancera


# Ansible 
## Ansible.cfg
```
[defaults]
remote_user = ubuntu

[inventory]
enable_plugins = gcp_compute
```
W **ansible.cfg** należy wskazać usera, którego klucz został dodany na maszynach, które będziemy konfigurować z wykorzystaniem Ansible. Należy również uruchomić plugin **gcp_compute**
## Dynamic Inventory
```
plugin: google.cloud.gcp_compute
projects:
    - project-alk-rb-01
filters:
    - status = RUNNING
keyed_groups:
    # Create groups from GCE labels
    - prefix: gcp
      key: labels

auth_kind: serviceaccount
service_account_file: ./credentials.json
```
Aby wykorzystać **dynamic inventory** konieczne jest wskazanie parametrów:
- plugin - plugin, który został uruchomiony w ansible.cfg
- projects - lista projektów, w których chcemy konfigurować maszyny 
- auth_kind - wskazujemy, że wykorzystamy konto serwisowe
- service_account_file - oraz wskazujemy ścieżkę do pliku credentials.json dla tego konta serwisowego
Dodatkowo wskazane parametry:
- filters - pozwala na wyfiltrowanie maszyn np na podstawie nazwy lub stanu w jakim się znajdują
- keyed_groups - pozwala na grupowanie maszyn np tak jak w tym przypadku po labelkach. Pozwala to na uruchomienie playbooka np tylko dla tych maszyn które mają label "env: prod"

## Przykładowy playbook
```
---
- name: Update Nginx
  hosts: gcp_env_prod
  become: yes
  tasks:
    - name: Replace default Nginx index.html
      template:
        src: index.html.j2
        dest: /var/www/html/index.html

    - name: Restart Nginx service
      service:
        name: nginx
        state: restarted
```
Prosty przykład playbooka do update'u Nginx'a, który na podstawie template'u ustawia w index.html datę update'u. Konfigurowane są hosty w grupie **gcp_env_prod** - zgodnie z **keyed_groups** są to grupy tworzone z prefixem **gcp** oraz wartością z labelek