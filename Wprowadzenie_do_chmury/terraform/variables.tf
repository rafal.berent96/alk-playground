variable "username" {
  description = "User name on provisioned vm"
  type        = string

  default = "ubuntu"
}

variable "ssh_public_key_filepath" {
  description = "Filepath for ssh public key"
  type        = string

  default = "./keys/kluczyk.pub"
}

variable "ssh_private_key_filepath" {
  description = "Filepath for ssh public key"
  type        = string

  default = "./keys/prywatny_kluczyk"
}

variable "instance_count" {
  description = "amount of instances"
  type        = string

  default = "2"
}

variable "gcp_zone" {
  description = "zona gcp"
  type = string
  default = "us-central1-c" 
}
variable "gcp_region" {
  description = "region gcp"
  type = string
  default = "us-central1" 
}

variable "gcp_machine_type" {
  description = "maszyna gcp - rozmiar"
  type = string

  default = "e2-micro" 
}