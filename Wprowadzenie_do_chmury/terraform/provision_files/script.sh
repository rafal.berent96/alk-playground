#!/bin/bash
sudo apt-get update
sudo apt-get install -y nginx
server_name=$(hostname)
echo "Hello world $server_name!" | sudo tee /var/www/html/index.html
sudo systemctl restart nginx