output "external_vm_ip" {
  value = ["${google_compute_instance.alkdemo.*.network_interface.0.access_config.0.nat_ip}"]
}
output "external_lb_ip" {
  value = google_compute_forwarding_rule.forwarding_rule.ip_address
}

