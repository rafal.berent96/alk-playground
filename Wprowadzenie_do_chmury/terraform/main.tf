provider "google" {
  project = "project-alk-rb-01"
  credentials = file("./credentials.json")
  region  = var.gcp_region
  zone    = var.gcp_zone
}

resource "google_compute_instance" "alkdemo" {
  count        = var.instance_count
  name         = "alk-demo-${count.index}"
  machine_type = var.gcp_machine_type

  tags = ["alk", "http-server"]
  labels = {
    env = "prod"
  }
  boot_disk {
    auto_delete = true
    device_name = "alkdemo_${count.index}_disk"

    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
      size  = 30
      type  = "pd-standard"
    }
    
    mode = "READ_WRITE"
  }

  network_interface {
    network = google_compute_network.vpc_network.self_link
    access_config {
      // for public ip
    }
  }

  connection {
    type  = "ssh"
    user  = var.username
    host  = self.network_interface[0].access_config[0].nat_ip
    private_key = file(var.ssh_private_key_filepath)
  }

  provisioner "file" {
    source      = "./provision_files/script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "/tmp/script.sh",
    ]
  }

  metadata = {
    sshKeys = "${var.username}:${file(var.ssh_public_key_filepath)}"
  }
  allow_stopping_for_update = true
}

resource "google_compute_network" "vpc_network" {
  name                    = "terraform-network"
  auto_create_subnetworks = "true"
}

resource "google_compute_firewall" "allow_ssh_http" {
  name    = "allow-ssh-http"
  network = google_compute_network.vpc_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["22", "80"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_instance_group" "instance_group" {
  name        = "alkdemo-instance-group"
  description = "Alkdemo Instance Group"

  instances = google_compute_instance.alkdemo[*].self_link

  named_port {
    name   = "alkdemo-http"
    port   = 80
  }
}

############### LoadBalancer ##################
resource "google_compute_region_backend_service" "backend_service" {
  name        = "alkdemo-backend-service"
  description = "Alkdemo Backend Service for Load Balancer"

  port_name   = "alkdemo-http"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  locality_lb_policy = "ROUND_ROBIN"
  protocol    = "HTTP"
  timeout_sec = 30
  
  health_checks = [google_compute_region_health_check.health_check.self_link]
  backend {
    group = google_compute_instance_group.instance_group.self_link
    balancing_mode = "UTILIZATION"
    capacity_scaler = 1
    max_utilization = 0.8
  }
}

resource "google_compute_region_health_check" "health_check" {
  name               = "alkdemo-health-check"
  check_interval_sec = 10
  timeout_sec        = 10
  unhealthy_threshold = 10
  healthy_threshold   = 2
  
  http_health_check {
    port               = 80
  }
  log_config {
    enable = true
  }
}

# Create Proxy-only Subnet
resource "google_compute_subnetwork" "proxy_subnet" {
  name          = "alkdemo-proxy-subnet"
  ip_cidr_range = "10.0.0.0/24"
  purpose       = "REGIONAL_MANAGED_PROXY"
  role          = "ACTIVE"
  network       = google_compute_network.vpc_network.self_link
  region        = var.gcp_region
}
# Update Firewall Rule to allow traffic on port 80 from Proxy-only Subnet
resource "google_compute_firewall" "allow_lb_traffic" {
  name    = "allow-lb-traffic"
  network = google_compute_network.vpc_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = [google_compute_subnetwork.proxy_subnet.ip_cidr_range]
}
# Create URL Map
resource "google_compute_region_url_map" "url_map" {
  name        = "alkdemo-lb-url-map"
  default_service = google_compute_region_backend_service.backend_service.self_link
}

# Create Target HTTP Proxy
resource "google_compute_region_target_http_proxy" "target_proxy" {
  name        = "alkdemo-lb-target-proxy"
  url_map     = google_compute_region_url_map.url_map.self_link
  region      = var.gcp_region
}

# Create Forwarding Rule
resource "google_compute_forwarding_rule" "forwarding_rule" {
  name               = "alkdemo-lb-forwarding-rule"
  region             = var.gcp_region
  load_balancing_scheme = "EXTERNAL_MANAGED"
  target             = google_compute_region_target_http_proxy.target_proxy.self_link
  network            = google_compute_network.vpc_network.self_link
  ip_protocol        = "TCP"
  port_range         = "80"
}
